package be.bf.android.demoapp.ui.persistence;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AppCompatActivity;

import be.bf.android.demoapp.databinding.ActivityPersistenceBinding;

public class PersistenceActivity extends AppCompatActivity {

    private ActivityPersistenceBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPersistenceBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);

        pref.edit()
                .putString("username", "Flavian")
                .apply();
        
    }
}