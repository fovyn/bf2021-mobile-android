package be.bf.android.demoapp.ui.login;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import be.bf.android.demoapp.dal.UserDAO;
import be.bf.android.demoapp.databinding.ActivityLoginBinding;
import be.bf.android.demoapp.models.LoginModel;
import be.bf.android.demoapp.models.entities.User;

public class LoginActivity extends AppCompatActivity {
    public static final int REQUEST_CODE = 101;

    private ActivityLoginBinding binding;
    private LoginModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.signIn.setOnClickListener(this::onSignInAction);
        binding.reset.setOnClickListener(this::onResetAction);

        model = new LoginModel(binding.email, binding.password);
    }

    private void onSignInAction(View v) {
        try (UserDAO dao = new UserDAO(this)) {
            dao.openWritable();
            dao.insert(new User(binding.email.getText().toString(), binding.password.getText().toString()));
        }


        setResult(RESULT_OK);
        finish();
    }

    private void onResetAction(View v) {
        setResult(RESULT_CANCELED);
        finish();
    }
}