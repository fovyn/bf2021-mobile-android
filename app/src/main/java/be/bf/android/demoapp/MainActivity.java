package be.bf.android.demoapp;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import be.bf.android.demoapp.databinding.ActivityMainBinding;
import be.bf.android.demoapp.ui.Exo2Activity;
import be.bf.android.demoapp.ui.login.LoginActivity;
import be.bf.android.demoapp.ui.permission.Exo3Activity;
import be.bf.android.demoapp.ui.persistence.PersistenceActivity;

public class MainActivity extends AppCompatActivity {

//    private String password = "Flavian";

    private ActivityMainBinding binding;

    private ActivityResultLauncher<Intent> exo1Launcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        exo1Launcher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), this::exo1Result);

        binding.btnGoToExo1.setOnClickListener(this::exo1Listener);
        binding.btnGoToExo2.setOnClickListener(this::exo2Listener);
        binding.btnGoToExo3.setOnClickListener(this::exo3Listener);
        binding.btnGoToExo4.setOnClickListener(this::exo4Listener);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
//        preferences.getString("password", "blop");
        String password = preferences.getString("password", null);

        Log.d("BIBLOP", "onCreate: " + password);

    }

    private void exo4Listener(View view) {
        Intent intent = new Intent(MainActivity.this, PersistenceActivity.class);
        startActivity(intent);
    }

    private void exo3Listener(View view) {
        Intent intent = new Intent(MainActivity.this, Exo3Activity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            String data = intent.getStringExtra("data");
            binding.tvData.setText(data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                executeCallPhone();
            }
        }
    }

    private void exo1Result(ActivityResult result) {
        Log.d("MainActivity", "exo1Result: " + result);

        if (checkCallingPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            executeCallPhone();
            return;
        }
        requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 1);
    }

    private void executeCallPhone() {
        Uri uri = Uri.parse("tel:0473400396");
        Intent intent = new Intent(Intent.ACTION_CALL, uri);
        startActivity(intent);
    }


    private void exo1Listener(View view) {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        exo1Launcher.launch(intent);
    }

    private void exo2Listener(View view) {
        Intent intent = new Intent(MainActivity.this, Exo2Activity.class);
        startActivity(intent);
    }
}