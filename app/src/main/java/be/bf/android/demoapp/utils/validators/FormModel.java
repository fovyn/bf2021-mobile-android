package be.bf.android.demoapp.utils.validators;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.function.BiFunction;

public abstract class FormModel {
    public boolean isValid() {
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            for (Annotation annotation : field.getDeclaredAnnotations()) {
                BiFunction<Object, Field, Boolean> resolver = ValidatorResolver.fromAnnotation(annotation);
                if (!resolver.apply(this, field)) {
                    return false;
                }
            }
        }

        return true;
    }
}
