package be.bf.android.demoapp.dal;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.Closeable;
import java.util.Collections;
import java.util.List;

import be.bf.android.demoapp.models.entities.User;

public class UserDAO implements Closeable {

    public static final String CREATE_QUERY = "CREATE TABLE user(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, username VARCHAR(50) NOT NULL, password VARCHAR(50));";
    public static final String UPGRADE_QUERY = "DROP TABLE user;";

    private DbHelper helper;
    private SQLiteDatabase database;

    public UserDAO(Context context) {
        helper = new DbHelper(context);
    }

    public SQLiteDatabase openWritable() {
        return this.database = helper.getWritableDatabase();
    }

    public SQLiteDatabase openReadable() {
        return this.database = helper.getReadableDatabase();
    }

    @SuppressLint("Range")
    public User getUserfromCursor(Cursor cursor) {
        User user = new User();
        user.setUsername(cursor.getString(cursor.getColumnIndex("username")));
        user.setPassword(cursor.getString(cursor.getColumnIndex("password")));

        return user;
    }

    //Récupérer les données dans la base de données
    public List<User> findAll() {
        List<User> users = Collections.emptyList();
        //"SELECT username+ ' '+ password, username, password FROM user"
        Cursor cursor = this.database.query("user", null, null, null, null, null, null);

        cursor.moveToFirst();

        do {
            users.add(getUserfromCursor(cursor));
        } while (cursor.moveToNext());

        return users;
    }

    //Ajouter un utilisateur dans la db
    public long insert(User user) {
        ContentValues cv = new ContentValues();
        cv.put("username", user.getUsername());
        cv.put("password", user.getPassword());

        return this.database.insert("user", null, cv);
    }

    public int update(int id, User user) {
        ContentValues cv = new ContentValues();
        cv.put("username", user.getUsername());
        cv.put("password", user.getPassword());

        return this.database.update("user", cv, "id = ?", new String[]{String.valueOf(id)});
    }

//    public int updateAll(User user) {
//        ContentValues cv = new ContentValues();
//        cv.put("ssin", user.getSsin());
//
//
//        return this.database.update("user", cv, null, null);
//    }

    public int delete(int id) {
        return this.database.delete("user", "id = ?", new String[]{id + ""});
    }

    public void close() {
        database.close();
    }
}
