package be.bf.android.demoapp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import be.bf.android.demoapp.MainActivity;
import be.bf.android.demoapp.databinding.ActivityExo2Binding;

public class Exo2Activity extends AppCompatActivity {

    private ActivityExo2Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityExo2Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.btnGotToExo2.setOnClickListener(this::sendData);
    }

    private void sendData(View view) {
        Intent intent = new Intent(Exo2Activity.this, MainActivity.class);
        intent.putExtra("data", binding.data.getText().toString());
        startActivity(intent);
    }
}