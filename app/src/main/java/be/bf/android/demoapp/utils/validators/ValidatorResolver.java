package be.bf.android.demoapp.utils.validators;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.function.BiFunction;

public enum ValidatorResolver {
    REQUIRED(Required.class, ValidatorResolver::required),
    MIN_LENGTH(MinLength.class, ValidatorResolver::minLength),
    MIN(Min.class, ValidatorResolver::min);

    public Class<?> annotation;
    public BiFunction<Object, Field, Boolean> validator;

    ValidatorResolver(Class<?> annotation, BiFunction<Object, Field, Boolean> validator) {
        this.validator = validator;
        this.annotation = annotation;
    }

    private static Boolean min(Object o, Field field) {
        Integer value = null;
        try {
            value = field.getInt(o);
            Min minAnnotation = field.getAnnotation(Min.class);

            return value >= minAnnotation.value();
        } catch (IllegalAccessException e) {
            return false;
        }
    }

    private static boolean minLength(Object src, Field field) {
        try {
            String value = (String) field.get(src);
            MinLength minLength = field.getAnnotation(MinLength.class);
            return value.length() >= minLength.size();
        } catch (Exception e) {
            return false;
        }
    }

    private static boolean required(Object src, Field field) {
        try {
            Object value = field.get(src);
            if (field.getType() == String.class && value != null && value.equals("")) {
                return false;
            } else {
                return value != null;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public static BiFunction<Object, Field, Boolean> fromAnnotation(Annotation annotation) {
        for (ValidatorResolver resolver : ValidatorResolver.values()) {
            if (resolver.annotation.equals(annotation.annotationType())) {
                return resolver.validator;
            }
        }
        return null;
    }
}
