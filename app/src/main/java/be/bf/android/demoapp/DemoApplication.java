package be.bf.android.demoapp;

import android.app.Application;

public class DemoApplication extends Application {

    private String helloWorld = "Hello world";

    public String getHelloWorld() {
        return this.helloWorld;
    }
}
