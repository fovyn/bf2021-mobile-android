package be.bf.android.demoapp.ui.permission;

import android.Manifest.permission;
import android.app.SearchManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import be.bf.android.demoapp.databinding.ActivityExo3Binding;

public class Exo3Activity extends AppCompatActivity {

    private static final int PERMISSION_CALL_REQUEST = 201;
    private ActivityExo3Binding binding;

    private ActivityResultLauncher<String> callLauncher = registerForActivityResult(
            new ActivityResultContracts.RequestPermission(),
            this::grantedCall
    );

    private void grantedCall(Boolean isGranted) {
        if (isGranted) {
            callAction();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityExo3Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.btnCall.setOnClickListener(this::onCallAction);
        binding.btnSearch.setOnClickListener(this::onSearchAction);
    }

    private void onSearchAction(View view) {
        Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
        intent.putExtra(SearchManager.QUERY, binding.etWebSearch.getText().toString());
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            callAction();
        }

    }

    private void onCallAction(View v) {
        if (ActivityCompat.checkSelfPermission(Exo3Activity.this, permission.CALL_PHONE) == PackageManager.PERMISSION_DENIED) {
//            ActivityCompat.requestPermissions(Exo3Activity.this, new String[]{permission.CALL_PHONE}, PERMISSION_CALL_REQUEST);
            callLauncher.launch(permission.CALL_PHONE);
        } else {
            callAction();
        }
    }

    private void callAction() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + binding.etPhoneNumber.getText().toString()));
        startActivity(intent);
    }
}